<?php
error_reporting (0);
include_once ('src/crest.php');

#######################################################################
#########_формируем_массив_для_листинга_сделок_по_фильтру_#############
$getDate = getdate(mktime(0, 0, 0, date('m'), date('d') - 14, date('Y') - 1));
$dateFrom = $getDate['year'].'-'.$getDate['mon'].'-'.$getDate['mday'];
$dataFilter = array('filter' => array(
		'CLOSEDATE' => $dateFrom,
		'UF_CRM_1521100014436' => '51'
	),
	'select' => array('ID', 'CONTACT_ID')
);

#######################################################################
######################_получаем_листинг_сделок_########################
$totalDeal = CRest::call('crm.deal.list', $dataFilter);
// если сделок больше 50 идем по ветке batch, иначе deals присваиваем total \\
if ($totalDeal['total'] > 50) {
	$iteration = intval($total / 50) - 1;
	if ($iteration % 50 == 0) $iteration -= 1;
	for ($i = 0; $i < $iteration; $i++) {
		$dataDeals[] = array(
			'method' => 'crm.deal.list',
			'params' => array(
				'filter' => array('>=CLOSEDATE' => $dateFrom, 'UF_CRM_1521100014436' => '51'),
				'select' => array('ID', 'CONTACT_ID'),
				'start'  => $start
			)
		);
	}
}
$deals = !empty($dataDeals) ? $dataDeals : $totalDeal;

#######################################################################
############# создаем массив для создания новых сделок ################
for ($i = 0; $i < count($deals['result']); $i++) {
	$newDealsAdd[] = array('CONTACT_ID' => $deals['result'][$i]['CONTACT_ID'], 
		'UF_CRM_1568145689' => 'Повторная сделка', 'UF_CRM_1521100014436' => '51');
}

if (count($newDealsAdd) > 50) {
	$newDealsAdd = array_chunk($newDealsAdd, 50);
	for ($i = 0; $i < count($newDealsAdd); $i++) {
		$newDeal = CRest::callBatch($newDealsAdd);
	}
} else {
	$newDeal = CRest::call('crm.deal.add', $newDealsAdd);
}
echo '<pre>';
print_r ($newDeal);
echo '</pre>';